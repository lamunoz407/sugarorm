package com.sena.sugarorm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;

public class ViewPagerActivity extends AppCompatActivity implements ListPositionsFragment.MarkerFromListToMap {

    private TabLayout tabs;
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        tabs = findViewById(R.id.tabs);
        viewPager = findViewById(R.id.viewPager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AddPositionFragment(), "Add");
        adapter.addFragment(new ListPositionsFragment(), "List");
        adapter.addFragment(new ListPositionsRecyclerviewFragment(), "Recycler View");
        adapter.addFragment(new MapsFragment(),"Map");
        viewPager.setAdapter(adapter);
        tabs.setupWithViewPager(viewPager);



    }

    @Override
    public void fixMarker(double latitude, double longitude) {
        MapsFragment.latitude=latitude;
        MapsFragment.longitude=longitude;
        viewPager.setCurrentItem(3);
    }
}