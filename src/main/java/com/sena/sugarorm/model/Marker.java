package com.sena.sugarorm.model;

import com.orm.SugarRecord;

public class Marker extends SugarRecord {


    private double latitude, longitude;
    private String tittle, description, image_url;

    public Marker() {
    }

    public Marker(double latitude, double longitude, String tittle, String description, String image_url) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.tittle = tittle;
        this.description = description;
        this.image_url = image_url;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public String toString() {
        return  tittle +
                "\n" + description +
                "\n" + latitude +
                "\n" + longitude;
    }
}
