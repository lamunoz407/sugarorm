package com.sena.sugarorm;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.sena.sugarorm.model.Marker;


public class AddPositionFragment extends Fragment implements View.OnClickListener {

    /*
    *   Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);*/
    private static final int REQUEST_CODE = 10;
    //My own variables
    private EditText title, description;
    private ImageView btn_save_location;
    private TextInputLayout frame_title, frame_description;
    private double longitude;
    private double latitude;
    //Location
    private LocationManager locationManager;
    private Location location;
    private LocationListener locationListener;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AddPositionFragment() {
        // Required empty public constructor
    }

    public static AddPositionFragment newInstance(String param1, String param2) {
        AddPositionFragment fragment = new AddPositionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                longitude = location.getLongitude();
                latitude = location.getLatitude();
                //Toast.makeText(getContext(), "Longitude "+longitude + " Latitude "+latitude, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {
                Toast.makeText(getContext(), "What that hell is going on?", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.e("DATA","VOY A " +
                        " QUE ENCIENDAN EL GPS");
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            Log.e("DATA","Pedir permisos");
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},10);

            return;
        }
        location = locationManager.getLastKnownLocation(locationManager.NETWORK_PROVIDER);
        //askLocation();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,0,locationListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(locationListener);
    }

    private void askLocation() {

        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        Log.e("DATA","Longitude "+longitude + " Latitude "+latitude);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 10:
                Log.e("DATA","Volvi");
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_add_position, container, false);
        //Bind XML and Java files
        btn_save_location = view.findViewById(R.id.btn_save_location);
        btn_save_location.setOnClickListener(this);
        title = view.findViewById(R.id.et_title);
        description = view.findViewById(R.id.et_description);
        frame_title = view.findViewById(R.id.frame_title);
        frame_description = view.findViewById(R.id.frame_description);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save_location:
                if(textInputsAreOk()){
                    String stringTitle = title.getText().toString();
                    String stringDescription = description.getText().toString();
                    double latitude = getLatitude();
                    double longitude = getLongitude();
                    Marker objNewMarker = new Marker(latitude,longitude,stringTitle,stringDescription,"https://www.pngkit.com/png/full/431-4318851_choose-location-transparent-icon-png-location.png");
                    objNewMarker.save();
                    Toast.makeText(getContext(), getResources().getString(R.string.location_save), Toast.LENGTH_LONG).show();
                    cleanTextInputs();
                }
                break;
        }
    }

    private void cleanTextInputs() {
        title.setText("");
        description.setText("");
    }

    private double getLongitude() {
        return longitude;
    }

    private double getLatitude() {
        return latitude;
    }

    private boolean textInputsAreOk() {
        boolean areFieldsOk = true;
        String sTitle = title.getText().toString().trim();
        String sDescription = description.getText().toString().trim();
        if(sTitle.equals("")){
            frame_title.setError(getResources().getString(R.string.title_empty));
            areFieldsOk=false;
        }else{
            frame_title.setError(null);
        }
        if(sDescription.equals("")){
            frame_description.setError(getResources().getString(R.string.description_empty));
            areFieldsOk=false;
        }else {
            frame_description.setError(null);
        }
        return areFieldsOk;
    }


/*
    @Override
    public void onLocationChanged(Location location) {
        double longitude = location.getLongitude();
        double latitude = location.getLatitude();
        Toast.makeText(getContext(), "Longitude "+longitude + " Latitude "+latitude, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }*/
}