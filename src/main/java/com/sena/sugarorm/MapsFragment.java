package com.sena.sugarorm;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sena.sugarorm.model.Marker;

import java.util.ArrayList;
import java.util.List;

public class MapsFragment extends Fragment{

    public static double  latitude = 0;
    public static double longitude = 0;
    List<Marker> markers;
    private OnMapReadyCallback callback = new OnMapReadyCallback() {

        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        @Override
        public void onMapReady(GoogleMap googleMap) {
            Log.e("DATA","google maps");

            //I add all markers that i have in my DB
            markers = new ArrayList<>();
            markers = Marker.listAll(Marker.class);
            for (Marker myMarker: markers) {
                LatLng latLng = new LatLng(myMarker.getLatitude(), myMarker.getLongitude());
                googleMap.addMarker(new MarkerOptions().position(latLng).title(myMarker.getTittle()));
            }

            //googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 15.0f));
           // googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Marker in Sydney"));

        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }

    }
/*
    @Override
    public void onResume() {
        super.onResume();
        Log.e("DATA","onResume Maps");
        markers = new ArrayList<>();
        markers = Marker.listAll(Marker.class);
    }

*/
}