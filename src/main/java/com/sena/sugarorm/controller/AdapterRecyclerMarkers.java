package com.sena.sugarorm.controller;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sena.sugarorm.R;
import com.sena.sugarorm.model.Marker;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterRecyclerMarkers extends RecyclerView.Adapter<AdapterRecyclerMarkers.myHolder> {


    List<Marker> markers;

    public AdapterRecyclerMarkers(List<Marker> markers) {
        this.markers = markers;
    }

    @NonNull
    @Override
    public myHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_markers, parent, false);
        myHolder myHolder = new myHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull myHolder holder, int position) {
        holder.title.setText(markers.get(position).getTittle());
        holder.description.setText(markers.get(position).getDescription());

    }

    @Override
    public int getItemCount() {
        return markers.size();
    }

    public class myHolder extends RecyclerView.ViewHolder {
        public ImageView imageViewPlace, btn_delete, btn_update;
        public TextView title, description;
        public CardView cardView;

        public myHolder(@NonNull View itemView) {
            super(itemView);
            imageViewPlace = itemView.findViewById(R.id.img_cv);
            btn_delete = itemView.findViewById(R.id.btn_cv_delete);
            btn_update = itemView.findViewById(R.id.btn_cv_update);
            title = itemView.findViewById(R.id.tv_cv_title);
            description = itemView.findViewById(R.id.tv_cv_description);
            cardView = itemView.findViewById(R.id.cardView_detail);
        }
    }
}
