package com.sena.sugarorm;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sena.sugarorm.controller.AdapterRecyclerMarkers;
import com.sena.sugarorm.model.Marker;

import java.util.ArrayList;


public class ListPositionsRecyclerviewFragment extends Fragment {


    RecyclerView objRecyclerView;
    AdapterRecyclerMarkers adapterRecyclerMarkers;
    RecyclerView.LayoutManager objLayoutManager;


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ListPositionsRecyclerviewFragment() {
        // Required empty public constructor
    }

    public static ListPositionsRecyclerviewFragment newInstance(String param1, String param2) {
        ListPositionsRecyclerviewFragment fragment = new ListPositionsRecyclerviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_list_positions_recyclerview, container, false);
        objLayoutManager = new LinearLayoutManager(getContext());
        objRecyclerView = view.findViewById(R.id.recycler);
        objRecyclerView.setHasFixedSize(false);
        objRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapterRecyclerMarkers = new AdapterRecyclerMarkers( Marker.listAll(Marker.class));
        objRecyclerView.setAdapter(adapterRecyclerMarkers);

        return view;
    }
}