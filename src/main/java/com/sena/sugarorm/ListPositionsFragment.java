package com.sena.sugarorm;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.sena.sugarorm.model.Marker;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListPositionsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListPositionsFragment extends Fragment {

    public interface MarkerFromListToMap{
        void fixMarker(double latitude, double longitude);
    }

    private MarkerFromListToMap mlistener;
    private ListView listView;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ListPositionsFragment() {
        // Required empty public constructor
    }

    public static ListPositionsFragment newInstance(String param1, String param2) {
        ListPositionsFragment fragment = new ListPositionsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


        Log.e("ListPosition", "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_list_positions, container, false);
        listView = view.findViewById(R.id.listView);
        Log.e("ListPosition", "onCreateView");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("ListPosition", "OnStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        List<Marker> markers = Marker.listAll(Marker.class);
        ArrayAdapter<Marker> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, markers);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), ""+parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                String data = parent.getItemAtPosition(position).toString();
                String arrayData[] = data.split("\n");
                mlistener.fixMarker(Double.parseDouble(arrayData[2]), Double.parseDouble(arrayData[3]));
            }
        });
        Log.e("ListPosition", "OnResume");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mlistener = (MarkerFromListToMap) context;
    }
}